# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.2.0] - 2017-05-28

### Added
- Group mode (ie. group of hosts can share a certificate and thus a private key)

## 0.1.0 - 2017-05-25

### Added
- The MIT License
- Host mode (ie. each managed host has its own private key and certificate)
- Project desciption and a user guide

[0.2.0]: https://gitlab.com/tomaskadlec/ansible-role-ssl/compare?from=v0.1.0&to=0.2.0
[Unreleased]: https://gitlab.com/tomaskadlec/ansible-role-ssl/compare?from=v0.2.0&to=master

<!--
## [Unreleased]
## [X.Y.Z] - YYYY-MM-DD

### Changed
- 

### Added
- 

### Fixed
- 

### Removed
-

[0.2.0]: https://gitlab.com/tomaskadlec/ansible-role-ssl/compare?from=v0.1.0&to=0.2.0
-->

<!--
vim : ft=md spelllang=en spell tabstop=2 shiftwidth=2 expandtab :
-->
