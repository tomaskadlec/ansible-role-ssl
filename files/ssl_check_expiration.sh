#!/bin/bash
# Return number of days a certificate expires in

cert_end_date=$(openssl x509 -in <(echo -e "$SSL_CERT") -enddate -noout | cut -d'=' -f2)
cert_remaining_time=$(date -d "$cert_end_date" "+%s");
current_time=$(date "+%s"); 
echo $((($cert_remaining_time - $current_time) / 60 / 60 / 24));
