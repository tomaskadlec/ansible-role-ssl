.. references.rst
.. This file lists all external references used in the project documentation.

.. _`Ansible`: http://docs.ansible.com/
.. _`Ansible Vault`: http://docs.ansible.com/ansible/playbooks_vault.html

.. _`Let's Encrypt`: https://letsencrypt.org/
.. _`Let's Encrypt chain of trust`: https://letsencrypt.org/certificates/

.. _`merge dicts`: http://docs.ansible.com/ansible/intro_configuration.html#hash-behaviour
