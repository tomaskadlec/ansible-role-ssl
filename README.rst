ansible-role-ssl
================

.. include:: references.rst

This project is an Ansible_ role that handles SSL/TLS keys and certificates.

* Private key management:

  * create private key locally on a controller machine
  * store private key encrypted within a project (e.g. on a controller machine)
  * deploy private key to a managed host

* Self-signed certificate management:

  * create self-signed certificate locally on a controller machine
  * store self-signed certificate within a project
  * deploy self-signed certificate to a managed host

* Manage certificates issued by `Let's Encrypt`_ certificate authority:

  * create account private key locally on a controller machine
  * store account private key encrypted within a project (e.g. on a controller machine)
  * deploy account private key to a managed host
  * optionally install and configure web server needed to fulfill http-01 ACME protocol challenge
  * obtain new or renew a certificate using http-01 ACME protocol challenge
  * supports staging (default) and production `Let's Encrypt`_ ACME protocol endpoints

* Private keys are encrypted using `Ansible Vault`_.
* Group mode, ie. one private key and certificate is used by multiple servers

Supported distributions
-----------------------

* Debian GNU/Linux

Requirements
------------

* `Ansible`_ 2.2 and higher
* `Ansible`_ must be configured to `merge dicts`_, i.e. ``hash_behaviour = merge``
* `Ansible Vault`_ must be configured

Installation
------------

There are two possibilities how to install the roles. Choose one that suits you best.

#. Copy them into the ``roles`` subdirectory: ::

    cd YOUR_PROJECT
    mkdir -p roles/ssl
    wget -O - \
        https://gitlab.com/tomaskadlec/role-ssl/repository/archive.tar.bz2 \
        | tar xjf - --wildcards -C roles/pam-access --strip=1

#. If your project uses git you can make use of git submodules: ::

     cd YOUR_PROJECT
     mkdir -p roles
     git submodule add git@gitlab.com:tomaskadlec/ansible-role-ssl.git roles/ssl

Operations overview
-------------------

This section shows typical scenarios.

Self-signed certificates
~~~~~~~~~~~~~~~~~~~~~~~~

#. Private key is created for each managed host.

   Keys are created on a controller machine and stored encrypted using `Ansible
   Vault`_ within your project directory. A directory
   ``host_vars/{{inventory_host}}`` is created and key is stored in file called
   ``ssl_key.yml``.

#. Self-signed certificate is issued for each managed host.

   Certificates are created on a controller machine and stored within your
   project directory. A directory ``host_vars/{{inventory_host}}`` is created
   and key is stored in file called ``ssl_self_signed_cert.yml``.

#. Private key and self signed certificate are deployed to each managed host.
#. Optionally, a task may be planned (``cron.daily``) to handle renewal of the
   certificate.
   
Let's Encrypt
~~~~~~~~~~~~~

#. Private key is created for each managed host.

   Keys are created on a controller machine and stored encrypted using `Ansible
   Vault`_ within your project directory. A directory
   ``host_vars/{{inventory_host}}`` is created and key is stored in file called
   ``ssl_key.yml``.

#. Private key is deployed to each managed host.
#. Account private key (`Let's Encrypt`_ account) is created for each managed host.

   Keys are created on a controller machine and stored encrypted using `Ansible
   Vault`_ within your project directory. A directory
   ``host_vars/{{inventory_host}}`` is created and key is stored in file called
   ``ssl_lets_encrypt_user_key.yml``.

#. Account private key is deployed to each managed host.
#. Each managed host than follows ACME challenge-response protocol and obtains
   a certificate from `Let's Encrypt`_.
#. Optionally, a task may be planned (``cron.daily``) to handle renewal of the
   certificate.

Certificate renewal
~~~~~~~~~~~~~~~~~~~

A recommended way to renew certificates (both self-signed end `Let's Encrypt`_)
is to use this role again. It might not be possible to install Ansible_ to
managed hosts and if it is installed it needs to access project repository and
know `Ansible Vault`_ secret key. Thus, triggering a CI job is considered as
preferred way to renew certificates. You might decide to create a separate
playbook to run just this role and nothing else.

Group mode
~~~~~~~~~~

If multiple hosts must share a certificate and thus a private key there is a
*group mode*. It is an opposite to default *host mode*. This is advanced (and
obscure) feature. Mostly, it is not needed!

I know only one use case for it. OpenVPN client can select a server to connect
to from a pool but it does not check server name against common name or
alternate names in certificate by default. It may be a security issue so it is
wise to configure it to do so. However, OpenVPN supports only one CN to
validate, even if server is chosen from a pool of available servers.  Elegant
solution is to use a single certificate with one common name which is a group
name and server names are certified as alternate names.

If *group mode* is enabled the role operates in similar manner but there are
differences:

* Tasks are delegated to the first host in a group if they are supposed to run
  just once (e.g. creating a private key). If it is necessary results of such
  tasks are copied to the rest of the hosts in the group. 
* The first host if chosen by Ansible_ and may differ between subsequent
  executions. It's not something one can rely on.
* Private key, certificate (self-signed mode) or private account key (`Let's
  Encrypt`_ mode) are stored within the project in ``group_vars/{{ssl_group}}``
  directory. Filenames are the same as in *host mode*.

Configuration
-------------

The role requires a bit of configuration to be in place in the first place.

Configuration is divided into *user configuration* and *detailed
configuration*. Changes in *detailed configuration* should not be necessary and
are considered dangerous (i.e. don't do it unless you know exactly what you are
doing).

All *user configuration* variables follows.

``ssl_mode`` ``self-signed|lets_encrypt`` (required)
    which certificates create/renew self-signed or let's encrypt

``ssl_cn`` (required)
    certified common name (i.e. server name)

``ssl_alt`` (default: empty list)
    a list of certified alternate DNS names

``ssl_dir`` (default: ``/etc/ssl/server``)
    a directory on managed host where keys and certificates will be stored

``ssl_renewal`` (default: 14)
    certificate should be renewed number of days before it expires

``ssl_renewal_script`` (default: ``None``)
    path to a template of a script that should be called to renew a certificate
    (ie. it should be located under ``templates`` directory of your project)

``ssl_lets_encrypt_mail`` (required)
    e-mail contact for notifications

``ssl_lets_encrypt`` ``staging|production`` (default: ``staging``)
    production or staging CA

``ssl_lets_encrypt_http_root`` (default: ``None``)
    challenge response directory 
    
    If a web server is running on a managed host and is listening on tcp/80 a
    response to http-01 ACME challenge will be stored within its web root
    directory. If this variable is empty a temporary web server will be started
    to serve a response to the challenge.

``ssl_vault_password_file`` (default: ``None``)
    path to vault password file

    Ansible_ sometimes works with path in an unexpected way (e.g. if you
    include whole playbook from another directory). Thus this option. Same
    applies for roles, files, templates and group and host variables. Those
    must be solved differently, e.g. by symlinks.

``ssl_group``
    name of group of hosts that share private key and certificates

    This is advanced and obscure feature. Please read section *Group mode*
    above before using it.

Usage
-----

To use the role just include it to your playbook. Assuming configuration is in
place you can run in as shown bellow. ::

    -   name: "Private key and self-signed certificate
        hosts: all
        roles:
            - {role: ssl}

The role uses internal data structure stored under ``ssl`` variable in facts.
A short list of the most useful variables follow. They can be used in
subsequent tasks and plays.  Value may be set according to ``ssl_mode`` chosen.

``ssl.dir``
    a directory on managed host where keys and certificates and other files
    were be stored

``ssl.key_name``
    filename of a file that contains private key

``ssl.dh_name`` 
    filename of a file that contains DH parameters

``ssl.chain_name``
    filename of a file that contains CA certificate chain of trust

``ssl.self_signed.cert_name`` (self-signed mode only)
    filename of a file on managed host that contains certificate

``ssl.lets_encrypt.cert_name`` (`Let's Encrypt`_ mode only)
    filename of a file on managed host that contains certificate

``ssl.lets_encrypt.user_key_name`` (`Let's Encrypt`_ mode only)
    filename of a file on managed host that contains private account key

Several (simple) variables are set as facts during execution:

``ssl_key``
    private key

``ssl_cert_name``
    filename of a file on managed host that contains certificate 

``ssl_cert`` (self-signed mode only)
    certificate

``ssl_lets_encrypt_user_key`` (`Let's Encrypt`_ mode only)
    private account key

``ssl_lets_encrypt_cert`` (`Let's Encrypt`_ mode only)
    certificate issued by `Let's Encrypt`_ certificate authority

License
-------

.. _`The MIT License`: LICENSE

This project is licensed under `The MIT License`_.

.. vim: spelllang=en spell:    
